var name;
//Función anónima
let a=function (mensaje){
    
    console.warn(mensaje);

    return true;
}
//Función callback
function userName(callback){
    var name = prompt("¿Cuál es tu nombre?");
    callback(name);
}
function buildGreting(name){
    alert('Hola, revisa la consola, '+ name);
}
userName (buildGreting);
//Función flecha + anónima
var arrayContatenado = (array1, array2) => array1.concat(array2);
console.log(arrayContatenado([1,2],[3,4,5]));